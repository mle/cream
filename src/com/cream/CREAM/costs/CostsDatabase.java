package com.cream.CREAM.costs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The database that stores the expenses for the user
 */
public class CostsDatabase {
    /**
     * name of the database
     */
    public static final String DATABASE_NAME = "CREAM_finances.db";

    /**
     * which version of the database to run
     */
    public static final int DATABASE_VERSION = 1;

    /**
     * name of the table for the data
     */
    public static final String TABLE_NAME = "costs";

    /**
     * Equivalent to has defines
     */
    interface Columns {
        String _ID = "cost_id";
        String COST = "cost";
        String TIMESTAMP = "timestamp";
        String CATEGORY = "category";
        String NOTE = "note";
    }

    /**
     * The database in question
     */
    private SQLiteDatabase db;

    /**
     * Database helper to open, close and write to database
     */
    private DatabaseHelper dbHelper;

    /**
     * Constructor
     *
     * @param context context which uses this object
     */
    public CostsDatabase(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    /**
     * Attempt to open the database
     *
     * @throws SQLException
     */
    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    /**
     * Close the database
     */
    public void close() {
        dbHelper.close();
    }

    /**
     * Add an entry into the database
     *
     * @param cost The cost item to add into the database
     * @return
     */
    public long addCost(CostItem cost) {
        ContentValues values = new ContentValues();
        values.put(Columns.COST, cost.cost);
        values.put(Columns.CATEGORY, cost.category);
        values.put(Columns.NOTE, cost.note);
        values.put(Columns.TIMESTAMP, cost.timestamp);
        return db.insert(TABLE_NAME, null, values);
    }

    /**
     * Delete an item from the database
     *
     * @param cost The cost item to delete
     */
    public void deleteCost(CostItem cost) {
        db.delete(TABLE_NAME, Columns._ID + " = " + cost.id, null);
    }

    /**
     * Retrieve a cost item from the database based on its id
     *
     * @param id the id of the item to search for
     * @return item called
     */
    public CostItem fetchCost(long id) {
        Cursor cursor = db.query(true, TABLE_NAME,
                new String[] {Columns._ID, Columns.COST, Columns.CATEGORY, Columns.NOTE, Columns.TIMESTAMP},
                Columns._ID + " = " + id, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursorToCost(cursor);
    }

    /**
     * Retrieves a list for cost items based on its category
     *
     * @param category category to search items for
     * @return list of all items of particular category
     */
    public List<CostItem> searchByCategory(String category) {
        List<CostItem> costs = new ArrayList<CostItem>();

        Cursor cursor = db.query(true, TABLE_NAME,
                new String[] {Columns._ID, Columns.COST, Columns.CATEGORY, Columns.NOTE, Columns.TIMESTAMP},
                Columns.CATEGORY + " ='" + category +"'", null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            costs.add(cursorToCost(cursor));
            cursor.moveToNext();
        }
        Collections.reverse(costs);

        cursor.close();
        return costs;
    }

    /**
     * Retrieves all current entries in the database
     *
     * @return a list of all entries currently in the database
     */
    public List<CostItem> getAllCosts() {
        List<CostItem> costs = new ArrayList<CostItem>();

        Cursor cursor = db.query(TABLE_NAME,
                new String[] {Columns._ID, Columns.COST, Columns.CATEGORY, Columns.NOTE, Columns.TIMESTAMP},
                null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            costs.add(cursorToCost(cursor));
            cursor.moveToNext();
        }
        // update the list to show newest entries first
        Collections.reverse(costs);

        cursor.close();
        return costs;
    }

    /**
     * Converts a cursor to a more meaningful cost item object
     *
     * @param cursor cursor returned by querying database
     * @return cost item object
     */
    private CostItem cursorToCost(Cursor cursor) {
        CostItem cost = new CostItem();
        cost.id = cursor.getLong(0);
        cost.cost = cursor.getLong(1);
        cost.category = cursor.getString(2);
        cost.note = cursor.getString(3);
        cost.timestamp = cursor.getLong(4);
        return cost;
    }

    /**
     * Class used to help create the database
     */
    public static class DatabaseHelper extends SQLiteOpenHelper {

        /**
         * Constructor
         *
         * @param context context that calls this
         */
        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        /**
         * Creates the database
         *
         * @param db
         */
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TABLE_NAME + " ("
                    + Columns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + Columns.COST + " LONG NOT NULL, "
                    + Columns.TIMESTAMP + " LONG NOT NULL, "
                    + Columns.CATEGORY + " TEXT NOT NULL, "
                    + Columns.NOTE + " TEXT "
                    + ")"
            );
        }

        /**
         * Updates the database on upgrade of the database
         *
         * @param db database to update
         * @param oldVersion the old version of the database
         * @param newVersion new version
         */
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(DatabaseHelper.class.getName(),
                    "Upgrading database from version " + oldVersion + " to "
                            + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }
}
