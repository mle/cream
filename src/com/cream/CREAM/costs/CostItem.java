package com.cream.CREAM.costs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * An object for storing items in the database as a cost
 */
public class CostItem {
    /**
     * Id to index where in database item is held
     */
    public long id;

    /**
     * How much the item costed
     */
    public long cost;

    /**
     * Date of entry stored in epoch time
     */
    public long timestamp;

    /**
     * Category of describing the cost's usage
     */
    public String category;

    /**
     * Any extraneous notes
     */
    public String note;

    /**
     * Constructor
     */
    public CostItem() {}

    public CostItem(long cost, long timestamp, String category, String note) {
        this.cost = cost;
        this.timestamp = timestamp;
        this.category = category;
        this.note = note;
    }

    /**
     * Converts epoch storage of the timestamp in a more readable Locale
     *
     * @return Readable locale date of the timestamp
     */
    public String convertDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy", Locale.US);
        String converted = sdf.format(timestamp);

        return converted;
    }

    public String toString() {
        return id + ", " +
               cost + ", " +
               timestamp + ", " +
               "\"" + category + "\"" + ", " +
               "\"" + note + "\"" ;
    }

    /**
     * Converts the database into CSV format for exporting
     *
     * @param costs list database to convert
     * @return CSV format of database
     */
    public static String listToCsv(List<CostItem> costs) {
        StringBuilder res = new StringBuilder("id, cost, timestamp, category, note\n");
        for (CostItem c: costs) {
            res.append(c.toString()+"\n");
        }
        return res.toString();
    }
}
