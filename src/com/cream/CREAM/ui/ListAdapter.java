package com.cream.CREAM.ui;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.cream.CREAM.R;
import com.cream.CREAM.costs.CostItem;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * A custom adapter class to reorder the items as used in the list view
 * to display an image and text in order for each list item
 */
public class ListAdapter extends ArrayAdapter<CostItem> {
    /**
     * The context that uses this adapter
     */
    private final Context context;

    /**
     * The cost list to display
     */
    private final List<CostItem> costList;

    /**
     * Sets the layout and default fields for this class
     * @param context context which calls this adapter
     * @param costList the list of costs
     */
    public ListAdapter(Context context, List<CostItem> costList){
        super(context, R.layout.cost_item_row, costList);
        this.context = context;
        this.costList = costList;
    }

    /**
     * Formats the view of the list to display an image, and formatted text
     *
     * @param position default android parameters
     * @param convertView default android parameters
     * @param parent default android parameters
     * @return
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.cost_item_row, parent, false);
        CostItem current = costList.get(position);

        TextView costText = (TextView) rowView.findViewById(R.id.row_cost);
        TextView timeText = (TextView) rowView.findViewById(R.id.row_timestamp);
        TextView categoryText = (TextView) rowView.findViewById(R.id.row_category);
        TextView noteText = (TextView) rowView.findViewById(R.id.row_notes);
        ImageView icon = (ImageView) rowView.findViewById(R.id.row_icon);

        DecimalFormat df = new DecimalFormat();
        String wholeNum = df.format(current.cost/100);
        df.applyPattern("00");
        String decimal = (current.cost%100 == 0)?"":"."+df.format(current.cost%100);

        costText.setText("-$"+wholeNum+decimal);
        costText.setTextColor(Color.RED);
        timeText.setText(current.convertDate());
        categoryText.setText(current.category);
        noteText.setText(current.note);

        switch(current.category){
            case "Bills":               icon.setImageResource(R.drawable.icontransparent_bills); break;
            case "Car":                 icon.setImageResource(R.drawable.icontransparent_car); break;
            case "Entertainment":       icon.setImageResource(R.drawable.icontransparent_entertainment); break;
            case "Family & Personal":   icon.setImageResource(R.drawable.icontransparent_personal); break;
            case "Food & Drinks":       icon.setImageResource(R.drawable.icontransparent_food); break;
            case "Home":                icon.setImageResource(R.drawable.icontransparent_home); break;
            case "Shopping":            icon.setImageResource(R.drawable.icontransparent_shopping); break;
            case "Travel":              icon.setImageResource(R.drawable.icontransparent_travel); break;
            case "Utilities":           icon.setImageResource(R.drawable.icontransparent_utilities); break;
        }

        return rowView;
    }

}
