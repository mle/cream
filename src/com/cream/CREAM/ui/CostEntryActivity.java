package com.cream.CREAM.ui;

import android.app.*;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.cream.CREAM.R;
import com.cream.CREAM.costs.CostItem;
import com.cream.CREAM.costs.CostsDatabase;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * The activity (screen displayed) to enter user costs into the database.
 * This class handles all the functionality for User Entry. A value other
 * than 0 must be entered in order to proceed to the second User Entry
 * screen.
 */
public class CostEntryActivity extends FragmentActivity {

    /**
     *  The current category selected by the user
     */
    private String selected;

    /**
     *  A list holding all the category buttons. This is used
     *  in order to select currently selected categories and
     *  deselect the others
     */
    private ArrayList<Integer> categoryButtons;

    /**
     *  Calendar to reference the date entered by the user
     */
    private Calendar calendar = Calendar.getInstance();

    /**
     * The default create method for android Activity.
     * Called when the activity is first created. Initializes
     * key values and layout to show.
     *
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        selected = "";

        categoryButtons = new ArrayList<Integer>();
        categoryButtons.add(R.id.category_bills);
        categoryButtons.add(R.id.category_car);
        categoryButtons.add(R.id.category_entertainment);
        categoryButtons.add(R.id.category_personal);
        categoryButtons.add(R.id.category_food);
        categoryButtons.add(R.id.category_home);
        categoryButtons.add(R.id.category_shopping);
        categoryButtons.add(R.id.category_travel);
        categoryButtons.add(R.id.category_utilities);

        overridePendingTransition(R.anim.top_in, R.anim.nothing);
        setContentView(R.layout.add_cost);
    }

    /**
     * The on click function for going to the next user entry screen.
     * A number greater than 0 must be entered in order to proceed.
     *
     * @param view Required by the Android software what view is
     *             calling this function
     */
    public void goNext(View view) {
        TextView mTextView = (TextView) findViewById(R.id.money);
        String mText = mTextView.getText().toString();
        double d = Double.parseDouble(mText);
        if(d == 0) {
            Toast.makeText(getApplicationContext(), "Please enter a value!",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.add_screen, new CategoriesFragment());
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

        ft.commit();
    }

    /**
     * The on click button called when the user presses the back button
     * on the second user entry screen. This takes the user back to the
     * previous screen for entering a value.
     *
     * @param view Required by the Android software what view is
     *             calling this function
     */
    public void goBack(View view){
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack();
    }

    /**
     * Used to pop up a display dialog so the user can set their own
     * date to detail the day the expense was on
     *
     * @param view Required by the Android software what view is
     *             calling this function
     */
    public void popupDialog(View view){

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "EEE, d MMM yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                TextView dateSelected = (TextView) findViewById(R.id.date_selected);
                dateSelected.setText(sdf.format(calendar.getTime()));
            }
        };

        DatePickerDialog dialog = new DatePickerDialog(CostEntryActivity.this, date,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.setTitle("Select Date");
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    /**
     * Sets the category button selected by the user as selected
     * to notify them of their selection
     *
     * @param id  The id of the button in the layout
     * @param category  Which category as a string, that was selected
     */
    public void setSelected (int id, String category) {
        for (int i : categoryButtons){
            Button b = (Button) findViewById(i);
            if(i == id) {
                b.setSelected(true);
            } else {
                b.setSelected(false);
            }
        }

        selected = category;
    }

    /**
     * The onclick button for each category button detailing
     * the description of the expense
     *
     * @param view  Required by the Android software what view is
     *             calling this function
     */
    public void selectCategory(View view){
        int id = view.getId();
        switch(id){
            case R.id.category_bills:            setSelected(id, "Bills"); break;
            case R.id.category_car:              setSelected(id, "Car"); break;
            case R.id.category_entertainment:    setSelected(id, "Entertainment"); break;
            case R.id.category_personal:         setSelected(id, "Family & Personal"); break;
            case R.id.category_food:             setSelected(id, "Food & Drinks"); break;
            case R.id.category_home:             setSelected(id, "Home"); break;
            case R.id.category_shopping:         setSelected(id, "Shopping"); break;
            case R.id.category_travel:           setSelected(id, "Travel"); break;
            case R.id.category_utilities:        setSelected(id, "Utilities"); break;
        }
    }

    /**
     * The onclick function to add all of the details as selected by the user
     * into the database
     *
     * @param view  Required by the Android software what view is
     *             calling this function
     */
    public void addToDatabase(View view){
        if(selected.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please select a category",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Calendar todayDate = Calendar.getInstance();
        long today = todayDate.getTimeInMillis();
        long timestamp = calendar.getTimeInMillis();
        if(timestamp > today){
            Toast.makeText(getApplicationContext(), "You are not from the future",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        TextView mTextView = (TextView) findViewById(R.id.money);
        String mText = mTextView.getText().toString();
        EditText noteText = (EditText) findViewById(R.id.notes);

        double d = Double.parseDouble(mText);
        long cost = (long) (d*100);
        String category = selected;
        String notes = noteText.getText().toString();

        CostItem cItem = new CostItem(cost, timestamp, category, notes);
        CostsDatabase db = new CostsDatabase(this);
        try {
            if (db != null) {
                db.open();
            }
        } catch (SQLException e) {
            Log.e("cream", "could not open database");
        }
        db.addCost(cItem);
        db.close();
        finish();
    }

    /**
     * The onclick button for which number key pressed and show it
     * accordingly on the screen
     *
     * @param view  Required by the Android software what view is
     *             calling this function
     */
    public void numkeyPressed(View view) {

        TextView mTextView = (TextView) findViewById(R.id.money);
        String mText = mTextView.getText().toString();
        String toAdd = "";

        switch(view.getId()){
            case R.id.button_0: toAdd += "0"; break;
            case R.id.button_1: toAdd += "1"; break;
            case R.id.button_2: toAdd += "2"; break;
            case R.id.button_3: toAdd += "3"; break;
            case R.id.button_4: toAdd += "4"; break;
            case R.id.button_5: toAdd += "5"; break;
            case R.id.button_6: toAdd += "6"; break;
            case R.id.button_7: toAdd += "7"; break;
            case R.id.button_8: toAdd += "8"; break;
            case R.id.button_9: toAdd += "9"; break;
            case R.id.button_dot: if(!mText.contains(".")) toAdd += "."; break;
            case R.id.button_back: if(mText.length() > 0) mTextView.setText(mText.substring(0, mText.length()-1)); break;
        }

        if(mText.length() >= 10) return;

        if(mText.equals("0") && !toAdd.equals(".")) {
            mTextView.setText(toAdd);
        } else if (mText.length() > 3
                && mText.charAt(mText.length()-3) == '.'
                && !toAdd.isEmpty()) {
            mTextView.setText(mText.substring(0, mText.length()-1)+toAdd);
        } else {
            mTextView.append(toAdd);
        }

    }

    /**
     * Used to navigate to the previous activity, The "home"
     * activity displaying the list view.
     */
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.nothing, R.anim.top_out);
    }

}