package com.cream.CREAM.ui;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.cream.CREAM.R;

import com.cream.CREAM.costs.CostItem;
import com.echo.holographlibrary.*;

import java.util.*;

/**
 * The activity to display the database in graph form
 */
public class GraphViewFragment extends Fragment {
    /**
     * The current list of costs
     */
    private List<CostItem> costs;

    /**
     * Default android function for creating an android activity
     *
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Returns the colour used for each particular category displayed
     * in the icon's background
     *
     * @param category
     * @return the colour used for a particular category
     */
    private int getCategoryColor(String category) {
        switch (category) {
            case "Bills": return Color.parseColor("#4D4D4D");
            case "Car": return Color.parseColor("#5DA5DA");
            case "Entertainment": return Color.parseColor("#FAA43A");
            case "Family & Personal": return Color.parseColor("#60BD68");
            case "Food & Drinks": return Color.parseColor("#F17CB0");
            case "Home": return Color.parseColor("#B2912F");
            case "Shopping": return Color.parseColor("#B276B2");
            case "Travel": return Color.parseColor("#DECF3F");
            case "Utilities": return Color.parseColor("#F15854");
        }
        return Color.parseColor("#000000");
    }

    /**
     * Default android function to create view. This initializes the
     * graphs to show on screen.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.graph_view,
                container, false);

        Line l = new Line();
        long max = 0;
        long days[] = new long[7];
        Arrays.fill(days, 0);
        long DAY_IN_MS = 1000 * 60 * 60 * 24;
        long now = System.currentTimeMillis();
        for (CostItem c: costs) {
            max = Math.max(c.cost, max);
            Date d = new Date(c.timestamp);
            long delta = now - d.getTime();
            if (delta < 7 * DAY_IN_MS) {
                days[7 - (int)(delta/DAY_IN_MS) - 1] += c.cost;
            }
        }

        for (int i = 0; i < 7; i++) {
            LinePoint p = new LinePoint();
            p.setX(i);
            p.setY(days[i]);
            l.addPoint(p);
        }

        l.setColor(Color.parseColor("#FFBB33"));

        LineGraph li = (LineGraph)view.findViewById(R.id.line);
        li.addLine(l);
        li.setRangeY(0, max);
        li.setLineToFill(0);

        PieGraph pg = (PieGraph)view.findViewById(R.id.pie_chart);
        Map<String, Long> values = new HashMap<>();
        for (CostItem c: costs) {
            if (values.get(c.category) != null) {
                values.put(c.category, values.get(c.category) + c.cost);
            } else {
                values.put(c.category, c.cost);
            }
        }
        for (String category: values.keySet()) {
            PieSlice slice = new PieSlice();
            slice.setValue(values.get(category));
            slice.setColor(getCategoryColor(category));
            slice.setTitle(category);
            pg.addSlice(slice);

        }
        return view;
    }

    /**
     * Set the cost list to display the graphs for
     *
     * @param costs list for showing graphs
     */
    public void setCosts(List<CostItem> costs) {
        this.costs = costs;
    }
}