package com.cream.CREAM.ui;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.cream.CREAM.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * This class describes the layout and initializations for the second
 * user entry screen.
 */
public class CategoriesFragment extends Fragment {

    /**
     * Default android function for creating a view. The parameters
     * describe default android parameters
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.add_cost2, container, false);

        TextView prevMoneyText = (TextView) getActivity().findViewById(R.id.money);
        TextView currMoneyText = (TextView) view.findViewById(R.id.money2);

        currMoneyText.setText(prevMoneyText.getText().toString());

        TextView selectedDate = (TextView) view.findViewById(R.id.date_selected);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy", Locale.US);

        selectedDate.setText(sdf.format(calendar.getTime()));

        return view;
    }

}