package com.cream.CREAM.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.cream.CREAM.R;
import com.cream.CREAM.costs.CostItem;
import com.cream.CREAM.costs.CostsDatabase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * The default activity to start up when the app first loads
 */
public class HomeActivity extends Activity {
    /**
     * The drawer to display categories to select what categories to show
     * on list
     */
    private DrawerLayout categoriesDrawerLayout;

    /**
     * The list view for the drawer
     */
    private ListView categoriesDrawerList;

    /**
     * The names to hold in the categories list
     */
    private String[] categoriesDrawerListNames;

    /**
     * Toggleable action bar drawer
     */
    private ActionBarDrawerToggle categoriesDrawerToggle;

    /**
     * Used to decide whether to show list or graphs
     */
    private boolean openGraph = false;

    /**
     * Menu
     */
    private Menu menu;

    /**
     * Default android function for creating activity. This initializes
     * the layout and default things to display in the drawer
     *
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        categoriesDrawerListNames = getResources().getStringArray(R.array.categories);

        categoriesDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        categoriesDrawerList = (ListView) findViewById(R.id.left_drawer);

        categoriesDrawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, categoriesDrawerListNames));

        categoriesDrawerToggle = new ActionBarDrawerToggle(this, categoriesDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close);

        categoriesDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        categoriesDrawerLayout.setDrawerListener(categoriesDrawerToggle);

        categoriesDrawerList.getLayoutParams().width = 350;

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, new CostListFragment());
        ft.commit();

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setTitle("ALL");


    }

    /**
     * Check to see whether certain buttons on the action bar are handled
     *
     * @param item the particular item to check
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (openGraph) {
            return true;
        }
        if (categoriesDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Transition to start activity for User Entry (Add costs)
     *
     * @param item default android parameter
     */
    public void slideCostEntryUp(MenuItem item) {
        startActivity(new Intent(this, CostEntryActivity.class));
    }

    /**
     * Switches between views of the list or the graphs. Displays
     * different things such as add/delete costs in list view or
     * export in graph views
     *
     * @param item default android parameter
     */
    public void toggleGraphView(MenuItem item) {
        if (openGraph) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, new CostListFragment());
            ft.commit();
            menu.findItem(R.id.action_delete).setVisible(true);
            menu.findItem(R.id.action_add).setVisible(true);
            menu.findItem(R.id.action_export).setVisible(false);
            menu.findItem(R.id.action_graph).setIcon(R.drawable.ic_action_graph);
            openGraph = false;
        } else {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            GraphViewFragment graphViewFragment = new GraphViewFragment();
            graphViewFragment.setCosts(getCostItems());
            ft.replace(R.id.content_frame, graphViewFragment);
            ft.commit();
            menu.findItem(R.id.action_delete).setVisible(false);
            menu.findItem(R.id.action_add).setVisible(false);
            menu.findItem(R.id.action_export).setVisible(true);
            menu.findItem(R.id.action_graph).setIcon(R.drawable.ic_action_list);
            categoriesDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            categoriesDrawerLayout.closeDrawer(categoriesDrawerList);
            openGraph = true;
        }
    }

    /**
     * Exports the database as a CSV to the root directory of the android app
     *
     * @param item default android parameter
     */
    public void exportCsv(MenuItem item) {
        CostsDatabase costsDatabase = new CostsDatabase(this);
        try {
            if (costsDatabase != null) {
                costsDatabase.open();
            }
        } catch (SQLException e) {
            Toast.makeText(getApplicationContext(), "Could not read database " + e.toString(), Toast.LENGTH_SHORT).show();
        }
        String csv = CostItem.listToCsv(costsDatabase.getAllCosts());
        String filename = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(Calendar.getInstance().getTime()) + "-cream.csv";
        File file = new File(Environment.getExternalStorageDirectory(), filename);
        try {
            FileOutputStream fos = new FileOutputStream(file);

            fos.write(csv.getBytes());
            fos.flush();
            fos.close();

            Toast.makeText(getApplicationContext(), "Wrote CSV to " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Could not write CSV: " + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Displays a popup to confirm if user wants to delete selected item from
     * the database, then deletes it if yes.
     *
     * @param item  default android parameter
     */
    public void deleteEntry(MenuItem item) {
        final CostListFragment costListFragment = (CostListFragment) getFragmentManager().findFragmentById(R.id.content_frame);
        if(!costListFragment.hasSelected()) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Delete Selected?");
        builder.setInverseBackgroundForced(true);
        builder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        costListFragment.deleteSelected();
                    }
                });
        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * creates the option bar menu
     *
     * @param menu default android parameter
     * @return
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        menu.findItem(R.id.action_delete).setVisible(true);
        menu.findItem(R.id.action_add).setVisible(true);
        menu.findItem(R.id.action_export).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Function to retrieve what sort of list to retrieve. An
     * overview, or search by category list
     *
     * @return type of list retrieved
     */
    public List<CostItem> getCostItems() {
        CostListFragment costListFragment = (CostListFragment) getFragmentManager().findFragmentById(R.id.content_frame);
        return costListFragment.getList();
    }

    /**
     * The onclick listener to retrieve which search by list to retrieve.
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView textView = (TextView) view;
            String category = textView.getText().toString();

            CostListFragment costListFragment = (CostListFragment) getFragmentManager().findFragmentById(R.id.content_frame);

            getActionBar().setTitle(category.toUpperCase());

            if(category.equals("All")){
                costListFragment.setAllCosts();
            } else {
                costListFragment.setListForCategory(category);
            }
        }

    }

    /**
     * Synchronizes the state of the activity
     */
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        categoriesDrawerToggle.syncState();
    }

    /**
     * Notifies the drawer on configurations changing
     */
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        categoriesDrawerToggle.onConfigurationChanged(newConfig);

    }

}
