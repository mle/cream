package com.cream.CREAM.ui;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.ListView;
import com.cream.CREAM.costs.CostItem;
import com.cream.CREAM.costs.CostsDatabase;

import java.sql.SQLException;
import java.util.List;

/**
 * The List View activity to show an overview an tracking of
 * the user's expenses as a list. Displayed on first loading
 * of the app
 */
public class CostListFragment extends ListFragment {

    /**
     * The database to read and store information from
     */
    private CostsDatabase database;

    /**
     * Users can select an item in the list. This variable
     * describes the position of the item in the list
     */
    private int selectedPosition = -1;

    /**
     * Used to search for entries of a particular category
     */
    private String category;

    /**
     * Loads the database to read info from and into.
     */
    private void loadDb() {
        try {
            if (database != null) {
                database.open();
            }
        } catch (SQLException e) {
            Log.e("cream", "could not open database");
        }
    }

    /**
     * Default android function for creating this view. It initializes
     * which layout to show. The parameters describe default android
     * parameters.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(android.R.layout.list_content,
                container, false);

        return view;
    }

    /**
     * Called when this fragment is attached to an activity. It initializes
     * default display for what is shown onto the activity
     *
     * @param activity The activity calling this fragment
     */
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("cream", "attached!");
        database = new CostsDatabase(getActivity());

        loadDb();

        setAllCosts();
    }

    /**
     * Selects the item in the list and shows as selected to the user
     *
     * @param l the list view that displays the list
     * @param v the view that is selected
     * @param position position of the item
     * @param id id of the item
     */
    public void onListItemClick(ListView l, View v, int position, long id) {
        v.setSelected(true);
        selectedPosition = position;
    }

    /**
     * Describes if the user has selected something or not
     *
     * @return true if user has made selection, -1 if not
     */
    public boolean hasSelected () {
        return (selectedPosition == -1)?false:true;
    }

    /**
     * Takes the selected entry and deletes it from the database
     */
    public void deleteSelected(){
        if(selectedPosition == -1) return;
        ListAdapter adapter = (ListAdapter) getListAdapter();
        CostItem costItem = (CostItem) adapter.getItem(selectedPosition);
        database.deleteCost(costItem);
        adapter.remove(costItem);
        adapter.notifyDataSetChanged();
        selectedPosition = -1;
    }

    /**
     * Update what to show as the list on the screen u
     *
     * @param context the activity using this fragment
     * @param costList the list of entries to show
     */
    public void updateListAdapter(Context context, List<CostItem> costList) {
        ListAdapter adapter = new ListAdapter(context, costList);
        setListAdapter(adapter);
    }

    /**
     * Sets the current list to show the entries with the chosen category
     *
     * @param category The category to show entries for
     */
    public void setListForCategory(String category){
        this.category = category;
        updateListAdapter(getActivity(), database.searchByCategory(category));
    }

    /**
     * Sets the current list to display all entries
     */
    public void setAllCosts(){
        this.category = null;
        updateListAdapter(getActivity(), database.getAllCosts());
    }

    /**
     * Called when the activity becomes active again
     */
    public void onResume() {
        loadDb();

        setAllCosts();

        super.onResume();
    }

    /**
     * Called when the activity is paused
     */
    public void onPause() {
        database.close();
        super.onPause();
    }

    /**
     * Returns what type of list to return, category list or ALL list
     */
    public List<CostItem> getList() {
        if (category == null) {
            return database.getAllCosts();
        }
        return database.searchByCategory(category);

    }
}