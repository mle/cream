package com.cream.CREAM.ui;
import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;
import com.cream.CREAM.R;

/**
 * Launches the default splash screen when the app starts
 */
public class SplashScreen extends Activity {

    /**
     * Duration to show the splash in milliseconds
     */
    private static int SPLASH_TIME_OUT = 1000;

    /**
     * Default android function for creating activity. Run this
     * splash screen then move onto the next activity, the home activity
     *
     * @param savedInstanceState default android parameter
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
 
        new Handler().postDelayed(new Runnable() {
 
            @Override
            public void run() {
                Intent i = new Intent(SplashScreen.this, HomeActivity.class);
                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}